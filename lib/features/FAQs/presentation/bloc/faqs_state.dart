part of 'faqs_bloc.dart';

abstract class FaqsState extends Equatable {
  const FaqsState();  

  @override
  List<Object> get props => [];
}
class FaqsInitial extends FaqsState {}
