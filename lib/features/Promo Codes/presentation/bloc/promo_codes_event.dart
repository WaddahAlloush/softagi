part of 'promo_codes_bloc.dart';

abstract class PromoCodesEvent extends Equatable {
  const PromoCodesEvent();

  @override
  List<Object> get props => [];
}
