part of 'promo_codes_bloc.dart';

abstract class PromoCodesState extends Equatable {
  const PromoCodesState();  

  @override
  List<Object> get props => [];
}
class PromoCodesInitial extends PromoCodesState {}
