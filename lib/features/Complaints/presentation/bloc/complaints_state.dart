part of 'complaints_bloc.dart';

abstract class ComplaintsState extends Equatable {
  const ComplaintsState();  

  @override
  List<Object> get props => [];
}
class ComplaintsInitial extends ComplaintsState {}
