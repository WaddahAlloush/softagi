import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/login.dart';
import '../entities/register.dart';

abstract class UserRepository {
  Future<Either<Failure, Login>> loginUser(
      {required String email, required String password});
  Future<Either<Failure, Unit>> logoutUser(String token);
  Future<Either<Failure, Register>> registerUser({
    required String email,
    required String name,
    required String phone,
    required String image,
    required String password,
  });
}
