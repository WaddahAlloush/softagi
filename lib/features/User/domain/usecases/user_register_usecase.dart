
import 'package:dartz/dartz.dart';
import 'package:softag/features/User/domain/repositories/user_repository.dart';

import '../../../../core/error/failures.dart';
import '../entities/register.dart';

class RegisterUser {
  final UserRepository userRepository;
  RegisterUser({
    required this.userRepository,
  });
  Future<Either<Failure, Register>> registeruser({
    required String email,
    required String name,
    required String phone,
    required String image,
    required String password,
  }) async {
    return await userRepository.registerUser(
        email: email,
        name: name,
        phone: phone,
        password: password,
        image: image);
  }
}
