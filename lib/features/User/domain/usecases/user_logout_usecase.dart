// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:softag/features/User/domain/repositories/user_repository.dart';

import '../../../../core/error/failures.dart';

class LogoutUser {
  final UserRepository userRepository;
  LogoutUser({
    required this.userRepository,
  });
  Future<Either<Failure, Unit>> call({required String token}) async {
    return await userRepository.logoutUser(token);
  }
}
