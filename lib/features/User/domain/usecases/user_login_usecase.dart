
import 'package:dartz/dartz.dart';
import 'package:softag/features/User/domain/repositories/user_repository.dart';

import '../../../../core/error/failures.dart';
import '../entities/login.dart';

class LoginUser {
  final UserRepository userRepository;
  LoginUser({
    required this.userRepository,
  });
  Future<Either<Failure, Login>> loginuser({
    required String email,
    required String password,
  }) async {
    return await userRepository.loginUser(email: email, password: password);
  }
}
