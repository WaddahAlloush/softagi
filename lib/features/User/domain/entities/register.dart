import 'package:equatable/equatable.dart';

class Register extends Equatable {
  const Register({
    required this.status,
    required this.message,
    required this.data,
  });

  final bool status;
  final String message;
  final RegisterData data;

  @override
  List<Object?> get props => [status, message, data];
}

class RegisterData extends Equatable {
  const RegisterData({
    required this.name,
    required this.phone,
    required this.email,
    required this.id,
    required this.image,
    required this.token,
  });

  final String name;
  final String phone;
  final String email;
  final int id;
  final String image;
  final String token;

  @override
  List<Object?> get props => [id, name, email, phone, image, token];
}
