// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softag/core/Strings/constants.dart';
import 'package:softag/core/error/exception.dart';

import 'package:softag/features/User/data/models/login_model.dart';

abstract class UserLocalDataSource {
  Future<Unit> cachUser(LoginModel loginModel);
  Future<LoginModel> getCachedUser();
   
}

class UserLocalDataSourceImp implements UserLocalDataSource {
  final SharedPreferences sharedPreferences;
  UserLocalDataSourceImp({
    required this.sharedPreferences,
  });
  @override
  Future<Unit> cachUser(LoginModel loginModel) async {
    Map<String, dynamic> userToJson = loginModel.toJson();
    await sharedPreferences.setString(cachedUser, jsonEncode(userToJson));
    return Future.value(unit);
  }

  @override
  Future<LoginModel> getCachedUser() {
    final jsonString = sharedPreferences.getString(cachedUser);
    if (jsonString != null) {
      final decodedJson = jsonDecode(jsonString);
      LoginModel jsonToUser = LoginModel.fromJson(decodedJson);
      return Future.value(jsonToUser);
    }else{
      throw EmptyCachException();
    }
  }
}
