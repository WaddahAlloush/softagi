// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:softag/core/Strings/api_constants.dart';

import 'package:softag/features/User/data/models/login_model.dart';

import '../../../../core/error/exception.dart';
import '../models/register_model.dart';

abstract class UserRemoteDataSource {
  Future<LoginModel> requestLoginUser({
    required String email,
    required String password,
  });
  Future<RegisterModel> requestRegisterUser({
    required String email,
    required String name,
    required String phone,
    required String image,
    required String password,
  });
}

class UserRemoteDataSourceImp implements UserRemoteDataSource {
  final http.Client client;
  UserRemoteDataSourceImp({
    required this.client,
  });
  @override
  Future<LoginModel> requestLoginUser(
      {required String email, required String password}) async {
    var response = await client.post(
        Uri.parse("$baseURL$loginEP"),
        headers: {'lang': 'en', 'Content-Type': 'application/json'},
        body: json
            .encode(<String, String>{"email": email, "password": password}));
    if (response.statusCode == 200 || response.statusCode == 201) {
      return LoginModel.fromJson(jsonDecode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<RegisterModel> requestRegisterUser(
      {required String email,
      required String name,
      required String phone,
      required String image,
      required String password}) async {
    var response =
        await client.post(Uri.parse("$baseURL$registerEP"),
            headers: {'lang': 'en', 'Content-Type': 'application/json'},
            body: json.encode(<String, String>{
              "email": email,
              "password": password,
              "name": name,
              "phone": phone,
              "image": image
            }));
    if (response.statusCode == 200 || response.statusCode == 201) {
      return RegisterModel.fromJson(jsonDecode(response.body));
    } else {
      throw ServerException();
    }
  }
}
