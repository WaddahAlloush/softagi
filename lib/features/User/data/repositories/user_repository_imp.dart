// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import 'package:softag/core/Network/network_info.dart';
import 'package:softag/core/Network/user_token.dart';
import 'package:softag/core/error/exception.dart';
import 'package:softag/core/error/failures.dart';
import 'package:softag/features/User/data/datasources/user_local_datasource.dart';
import 'package:softag/features/User/data/datasources/user_remote_datasource.dart';
import 'package:softag/features/User/domain/entities/login.dart';
import 'package:softag/features/User/domain/entities/register.dart';
import 'package:softag/features/User/domain/repositories/user_repository.dart';

class UserRepositoryImp implements UserRepository {
  final UserRemoteDataSource remoteDataSource;
  final UserLocalDataSource localDataSource;
  final NetworkInfo networkInfo;
  final UserToken userToken;
  UserRepositoryImp({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
    required this.userToken,
  });
  @override
  Future<Either<Failure, Login>> loginUser(
      {required String email, required String password}) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await remoteDataSource.requestLoginUser(
            email: email, password: password);
        await localDataSource.cachUser(remoteData);
        await userToken.setUserToken(remoteData.data.token);
        return Right(remoteData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localData = await localDataSource.getCachedUser();
        return Right(localData);
      } on EmptyCachException {
        return Left(EmptyCachFailure());
      }
    }
  }

  @override
  Future<Either<Failure, Register>> registerUser(
      {required String email,
      required String name,
      required String phone,
      required String image,
      required String password}) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await remoteDataSource.requestRegisterUser(
            email: email,
            name: name,
            phone: phone,
            image: image,
            password: password);
        return Right(remoteData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
  
  @override
  Future<Either<Failure, Unit>> logoutUser(String token) {
    // TODO: implement logoutUser
    throw UnimplementedError();
  }
}
