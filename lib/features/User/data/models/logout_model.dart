import 'package:softag/features/User/domain/entities/logout.dart';

class LogoutModel extends Logout {
  LogoutModel(
      {required super.status, required super.message,});
  factory LogoutModel.fromJson(Map<String, dynamic> json) {
    return LogoutModel(
      status: json["status"],
      message: json["message"],
     
    );
  }
  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
       
      };
}


