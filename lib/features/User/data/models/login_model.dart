import '../../domain/entities/login.dart';

class LoginModel extends Login {
  const LoginModel(
      {required super.status, required super.message, required super.data});
  factory LoginModel.fromJson(Map<String, dynamic> json) {
    return LoginModel(
      status: json["status"],
      message: json["message"],
      data: LoginDataModel.fromJson(json["data"]),
    );
  }
  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data,
      };
}

class LoginDataModel extends LoginData {
  const LoginDataModel(
      {required super.id,
      required super.name,
      required super.email,
      required super.phone,
      required super.image,
      required super.points,
      required super.credit,
      required super.token});
  factory LoginDataModel.fromJson(Map<String, dynamic> json) => LoginDataModel(
        credit: json['credit'],
        points: json['points'],
        name: json["name"],
        phone: json["phone"],
        email: json["email"],
        id: json["id"],
        image: json["image"],
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "phone": phone,
        "email": email,
        "id": id,
        "image": image,
        "token": token,
      };
}
