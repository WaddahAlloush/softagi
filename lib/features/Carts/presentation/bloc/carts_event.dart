part of 'carts_bloc.dart';

abstract class CartsEvent extends Equatable {
  const CartsEvent();

  @override
  List<Object> get props => [];
}
