part of 'carts_bloc.dart';

abstract class CartsState extends Equatable {
  const CartsState();  

  @override
  List<Object> get props => [];
}
class CartsInitial extends CartsState {}
