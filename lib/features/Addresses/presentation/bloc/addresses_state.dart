part of 'addresses_bloc.dart';

abstract class AddressesState extends Equatable {
  const AddressesState();  

  @override
  List<Object> get props => [];
}
class AddressesInitial extends AddressesState {}
