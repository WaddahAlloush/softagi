part of 'banners_bloc.dart';

abstract class BannersState extends Equatable {
  const BannersState();  

  @override
  List<Object> get props => [];
}
class BannersInitial extends BannersState {}
