// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softag/core/Strings/constants.dart';

class UserToken extends Equatable {
  final SharedPreferences sharedPreferences;
  const UserToken({
    required this.sharedPreferences,
  });
  Future<void> setUserToken(String token) async {
    await sharedPreferences.setString(userToken, token);
  }
  Future<void> removeUserToken() async {
    await sharedPreferences.remove(userToken);
  }


  String getUserToken() {
    final token = sharedPreferences.getString(userToken);
    if (token != null) {
      return token;
    } else {
      return "No User";
    }
  }

  @override
  List<Object> get props => [sharedPreferences];
}
